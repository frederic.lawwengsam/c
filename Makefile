.PHONY : clean

SRC_FILES = $(wildcard *.c) 

RESET := $(shell tput -Txterm sgr0)
RED := $(shell tput -Txterm setaf 1)
GREEN := $(shell tput -Txterm setaf 2)
BOLD := $(shell tput bold)

all: clean $(patsubst %.c,%,$(SRC_FILES))

%: %.c
	@echo "${GREEN}[Compiling]${RESET}: Source file ${BOLD}$<${RESET}"
	@gcc $< -o ./bin/$@

clean:
	@echo "${RED}[Cleaning]${RESET}: Delete binary directory"
	@rm -rf ./bin
	@mkdir ./bin
