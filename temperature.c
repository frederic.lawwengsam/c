#include <stdio.h>
#define MIN 0
#define CELSIUS_STEP 5

float convertToFahr(float celsius){
    return ((9.0/5.0)*celsius) +  32;
}

float convertToCelsius(float fahr){
    return (5.0/9.0)*(fahr - 32);
}

int main(){
    float celsius;
    float fahr;

    printf("Convert celsius to fahrenheit:\n");
    for(celsius=MIN; celsius <= 40; celsius = celsius + CELSIUS_STEP){
        printf("%.2f celsius : %.2f fahrenheit\n", celsius, convertToFahr(celsius));    
    }
    printf("\n");
    printf("Convert fahrenheit to celsius:\n");
    for(fahr=MIN; fahr <= 300; fahr = fahr + 20){
        printf("%.2f fahrenheit : %.2f celsius\n", fahr, convertToCelsius(fahr));    
    }


}
